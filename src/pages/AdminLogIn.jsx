import HeaderBadge from "../components/HeaderBadge";
import AdminLogInForm from '../components/AdminLogInForm';
import { ToastContainer } from 'react-toastify';

const AdminLogIn = () => {
    return (
        <>
            <AdminLogInForm />
            <ToastContainer />
        </>
    )
}

export default AdminLogIn;