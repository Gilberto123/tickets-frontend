import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';


import AdminLogIn from './pages/AdminLogIn';
import Header from './components/Header'

function App() {
  return (
    <Router>

      <Switch>

        <Route path="/">
          <AdminLogIn />
        </Route>

      </Switch>

    </Router>
  )
}

export default App;
