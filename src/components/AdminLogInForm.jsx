import { useRef } from 'react';
import { toast } from 'react-toastify';
import toastConfig from '../config/toastConfig'
import 'react-toastify/dist/ReactToastify.css';

import config from '../config/config';

const AdminLogInForm = () => {

    const user = useRef('');
    const pass = useRef('');

    const logIn = () => {
        const cu = user.current.value;
        const cp = pass.current.value;
        
        if (cu !== config.admin) {
            toast.error('Usuario equivocado', toastConfig.error);
            return;
        };

        if (cp !== config.pass) {
            toast.error('Contraseña equivocada', toastConfig.error);
            return;
        };

    };

    return (
        <section className="section-newsletter d-flex align-items-center">
            <div className="container">
                <div className="section-content">
                    <h2>Tickets Admin LogIn</h2>
                    <div className="newsletter-form clearfix row d-flex justify-content-center align-items-around">
                        <input ref={user} className='col-md-8 mt-5' type="text" placeholder="Nombre de Usuario" />
                        <input ref={pass} className='col-md-8 mt-5' type="password" placeholder="Contraseña" />
                        <input onClick={logIn} type="submit" />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default AdminLogInForm;