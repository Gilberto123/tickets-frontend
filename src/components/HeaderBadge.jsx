const HeaderBadge = () => {
    return (
        <div className="top-header top-header-bg">
            <div className="container">
                <div className="row">
                    <div className="top-left">
                        <ul>
                            <li>
                                <a href="#">
                                    <i className="fa fa-phone" />
                                    +62274 889767
                                </a>
                            </li>
                            <li>
                                <a href="mailto:hello@myticket.com">
                                    <i className="fa fa-envelope-o" />
                                    hello@myticket.com
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="top-right">
                        <ul>
                            <li>
                                <a href="#">Sign In</a>
                            </li>
                            <li>
                                <a href="#">Sign Up</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HeaderBadge;