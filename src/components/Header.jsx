import HeaderBadge from './HeaderBadge';

const Header = () => {
    return (
        <header id="masthead" className="site-header fix-header header-1">
            <HeaderBadge />
            <div className="main-header">
                <div className="container">
                    <div className="row">
                        <div className="site-branding col-md-3">
                            <h1 className="site-title"><a href="homepage-1.html" title="myticket" rel="home"><img src="images/logo.png" alt="logo" /></a></h1>
                        </div>
                        <div className="col-md-9">
                            <nav id="site-navigation" className="navbar">
                                <div className="navbar-header">
                                    <div className="mobile-cart"><a href="#">0</a></div>
                                    <button type="button" className="navbar-toggle offcanvas-toggle pull-right" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas">
                                        <span className="sr-only">Toggle navigation</span>
                                        <span className="icon-bar" />
                                        <span className="icon-bar" />
                                        <span className="icon-bar" />
                                    </button>
                                </div>
                                <div className="navbar-offcanvas navbar-offcanvas-touch navbar-offcanvas-right" id="js-bootstrap-offcanvas">
                                    <button type="button" className="offcanvas-toggle closecanvas" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas">
                                        <i className="fa fa-times fa-2x" aria-hidden="true" />
                                    </button>
                                    <ul className="nav navbar-nav navbar-right">
                                        <li className="active"><a href="full-event-schedule.html">Schedule</a></li>
                                        <li><a href="artist-page.html">Concerts</a></li>
                                        <li><a href="upcoming-events.html">Sports</a></li>
                                        <li><a href="order-ticket-without-seat.html">Parties</a></li>
                                        <li><a href="event-by-category.html">Theater</a></li>
                                        <li><a href="gallery.html">Gallery</a></li>
                                        <li><a href="select-seat-2.html">Ticekts</a></li>
                                        <li className="cart"><a href="#">0</a></li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>

    )
}

export default Header;